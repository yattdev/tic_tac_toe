cx-Freeze==6.6
importlib-metadata==4.5.0
Pillow==8.0.1
pygame==2.0.1
tk==0.1.0
zipp==3.4.1

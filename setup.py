#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
# "packages": ["os"] is used as example only
build_exe_options = {"packages": ["os"], "excludes": ["tkinter",
                                                      "random",
                                                      "functools",
                                                      "copy"]}

# base="Win32GUI" should be used only for Windows GUI app
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name = "Tic Tac Toe",
    version = "0.1",
    description = "Jeu Tic Tac Toe!",
    #  options = {"build_exe": build_exe_options},
    executables = [Executable("code.py", base=base)]
)

import sqlite3
import logging

class databaseHandler:
    """
        Thread-safe logging handler for SQLite.
    """
    def __init__(self):
        self.conn = sqlite3.connect('lib/scores.db')
        self.conn = self.conn.cursor()
        self.conn.execute('''CREATE TABLE IF NOT EXISTS humanVAI
        (NAME           TEXT  PRIMARY KEY  NOT NULL,
        SCORE            INT);''')
        self.conn.execute('''CREATE TABLE IF NOT EXISTS humanVHuman
        (NAME           TEXT  PRIMARY KEY  NOT NULL,
        SCORE            INT);''')
        # self.conn.commit()
        self.tableName = ""

    def clearTable(self,tableCode):
        if tableCode==0:
            self.tableName = "humanVHuman"
        else:
            self.tableName = "humanVAI"
        self.conn.execute("DELETE FROM "+self.tableName)
        statement = "INSERT INTO " + self.tableName + " VALUES ('Draw',0)"
        self.conn.execute(statement)
        if tableCode==1:
            statement = "INSERT INTO " + self.tableName + " VALUES ('Computer',0)"
            self.conn.execute(statement)
        # self.conn.commit()

    def incrementScore(self,name,tableCode):
        if tableCode==0:
            self.tableName = "humanVHuman"
        else:
            self.tableName = "humanVAI"
        data = self.conn.execute("SELECT NAME, SCORE FROM "+ self.tableName +" WHERE NAME = '"+name+"'" )
        statement = "INSERT INTO " + self.tableName + " VALUES ('" + name + "',1) ON CONFLICT (NAME) DO UPDATE SET Score=((SELECT SCORE FROM "+ self.tableName +" WHERE NAME='"+ name +"')+1)"
        self.conn.execute(statement)
        # self.conn.commit()
        #  self.returnTable(1)

    def returnTable(self,tableCode):
        if tableCode==0:
            self.tableName = "humanVHuman"
        else:
            self.tableName = "humanVAI"
        statement = "Select NAME, SCORE from {} ORDER BY SCORE DESC".format(self.tableName)
        # print(statement)
        cursor = self.conn.execute(statement).fetchall()
        # print(cursor)
        return cursor

    def __del__(self):
        self.conn.close()
